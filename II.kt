import kotlinx.coroutines.*

suspend fun first(i: Int): Int{
    delay(1000L)
    return i
}

suspend fun second(i: Int): Int{
    delay(3000L)
    return i
}

suspend fun main() = runBlocking {
    val startTime = System.currentTimeMillis()
    val res = first(5)+second(6)
    val endTime = System.currentTimeMillis()-startTime
    println("Res: $res")
    println("Time: $endTime")

    //Время, которое выводится в консоль отличается, т.к.
    //если просто вызывать функции, то они выполняются последовательно.
    //При вызове же асинхронно они выполняются параллельно.
    //Поэтому в первом случае мы имели время в ~4 секунды,
    //а во втором ~3 секунды

    val startTime2 = System.currentTimeMillis()
    val one = async(){
        first(5)
    }
    val second = async() {
        second(6)
    }
    val res2 = one.await() + second.await()
    val endTime2 = System.currentTimeMillis()-startTime2
    println("Res: $res2")
    println("Time: $endTime2")
}